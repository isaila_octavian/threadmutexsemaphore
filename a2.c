#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "a2_helper.h"
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>

pthread_mutex_t mutex1=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex3=PTHREAD_MUTEX_INITIALIZER;
int i1=1,i2=1,i3=1,i4=1;
sem_t *Semafor1=NULL;
sem_t *Semafor2=NULL;

//pthread_t thread1[7];
//pthread_t thread2[6];
//pthread_t thread3[42];

void* functieThreadProces5( void * arg)
{
	//pthread_mutex_lock(& mutex1);
	int*a=(int*)arg;
	info(BEGIN,5,*a);
	info(END,5,*a);
	//pthread_mutex_unlock(& mutex1);

	return NULL;
}

void* functieThreadProces9( void * arg)
{
	
	int*a=(int*)arg;
	info(BEGIN,9,*a);
	info(END,9,*a);
	return NULL;

}

void* functieThreadProces2( void * arg)
{
	Semafor1 = sem_open("Procesul2.1", O_CREAT, 0600, 0);
	Semafor2 = sem_open("Procesul2.2", O_CREAT, 0600, 0);
	//int i=0;
	pthread_mutex_lock(& mutex3);
	
	//info(BEGIN,2,i3);
	
	
	if(i3==5)
	{
		sem_wait(Semafor1);
		sem_wait(Semafor2);
		i3++;
		//i=1;
	}
	else if(i3==2)
	{
		sem_post(Semafor1);
		sem_post(Semafor2);

		info(BEGIN,2,2);
		info(BEGIN,2,5);

		info(END,2,5);
		info(END,2,2);
		i3++;
		//i=1;
	}
	else if(i3!=2 || i3!=5){
	info(BEGIN,2,i3);
	info(END,2,i3);
	i3++;
	}
	
	sem_post(Semafor2);
	sem_post(Semafor1);
	

	sem_close(Semafor1);
	sem_close(Semafor2);
	pthread_mutex_unlock(& mutex3);

	return NULL;
}


int main()
{
    init();
    pid_t pid1=-1, pid2=-1, pid3=-1,pid4=-1,pid5=-1,pid6=-1,pid7=-1,pid8=-1;

    info(BEGIN, 1, 0);
    
    pid1=fork();
    //printf("%d\n",pid1);
    if(pid1==0)
    {
    	info(BEGIN, 4, 0);
    	info(END,4,0);
    	exit(4);
    	// incepem si inchidem procesul 4
    }	

    pid2=fork();
    if(pid2==0)
    {
    	info(BEGIN, 3, 0);
    	info(END, 3, 0);
    	exit(3);
    	//incepem si inchidem procesul 3
    }
    
    pid3=fork();
    if(pid3==0)
    {
    	info(BEGIN, 2, 0);

    	pthread_t thread2[6];
    	for (int i=0; i<5; i++)
    		pthread_create(&thread2[i], NULL, functieThreadProces2, &i);

    	for(int i=0;i<5;i++)
    		pthread_join(thread2[i], NULL);
    	sem_unlink("Procesul2.1");
    	sem_unlink("Procesul2.1");
    	pid4=fork();
    	if(pid4==0)
    	{
    		info(BEGIN, 7, 0);
    		info(END, 7, 0);
    		exit(7);
    		//incepem si inchidem procesul 7
    	}

    		pid5=fork();
    		if(pid5==0)
    		{
    		info(BEGIN, 5, 0);
    		
    		pthread_t thread3[42];
    		int a[42];
    			for (int i=0; i<41; i++)
    			{
    				a[i]=i+1;
    				pthread_create(&thread3[i], NULL, functieThreadProces5, &a[i]);
    			}

    			for(int i=0;i<41;i++)
    				pthread_join(thread3[i], NULL);
    		
    		pid6=fork();
    		if(pid6==0)
    		{
    		info(BEGIN, 9, 0);
    		int b[7];
    		pthread_t thread1[7];
    			for (int i=0; i<6; i++)
    			{
    				b[i]=i+1;
    				pthread_create(&thread1[i], NULL, functieThreadProces9, &b[i]);
    			}

    			for(int i=0;i<6;i++)
    				pthread_join(thread1[i], NULL);
			
    		
    		info(END, 9, 0);
    		exit(9);
    		//incepem si inchidem procesul 9
    		}

    		pid7=fork();
    		if(pid7==0)
    		{
    		info(BEGIN, 6, 0);

    		pid8=fork();
    		if(pid8==0)
    		{
    		info(BEGIN, 8, 0);
    		info(END, 8, 0);
    		exit(8);
    		//incepem si inchidem procesul 8
    		}
    		wait(NULL);
    		info(END, 6, 0);
    		exit(6);
    		//exit(9);
    		}
    		wait(NULL);
    		wait(NULL);
    		info(END, 5, 0);
    		exit(5);
    		//exit(7);
    		//incepem si inchidem procesul 5
    		}
    	wait(NULL);
    	wait(NULL);
    	info (END, 2, 0);
    	exit(2);
    }
    wait(NULL);
    wait(NULL);
    wait(NULL);
    info(END, 1, 0);
    //exit(1);

    return 0;
}